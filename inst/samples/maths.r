#!/usr/bin/env r

devtools::load_all("./commando",quiet=TRUE)

sqrtr <- com() %@% function(x){
    x = as.numeric(x)
    message("square root of ",x," is ",sqrt(x))
}

cuber <- com() %@% function(x){
    x = as.numeric(x)
    message(x," cubed is ",x*x*x)
}

cubint <- com() %@% function(`x:int`){
    x*x*x
}


cubage = function(x){
    x^3
}

main <- com(help="This is main") %@%
    function(x){
        message("Hello ",x," I am main!")
    }



commando::main()
